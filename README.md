# renrs

*(Work in progress 🚧)*

renrs (name subject to change) is a visual novel and dialog engine written in Rust. It is fully compatible with [Ren'Py language `.rpy` scripts](https://www.renpy.org/doc/html/language_basics.html), letting you migrate easily. The core engine is logic only, so you are free to integrate it into whatever front-end framework or game engine you like. If you are creating a simple visual novel with no complex gameplay elements, you can use renrs's official GUI to have an all-in-one engine solution. If you want to use renrs as a dialog system in a full game engine, you can use renrs's official Godot integration.

### Modules

- **WIP** `renrs` is the core engine and Ren'Py script interpreter.
- **WIP** `renrs-gui` is a graphics layer for simple visual novels.
- **PLANNED** `renrs-editor` is a route viewer and theme editor for `renrs-gui`.
- **PLANNED** `renrs-godot` is an integration for Godot for more advanced projects.
- **PLANNED** `renrs-hub` is a launcher for visual novels made in `renrs-gui`.
