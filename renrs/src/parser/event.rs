use debug_cell::RefCell;
use std::{collections::HashMap, rc::Rc};

use regex::Regex;

use super::{block::CommandBlock, token::Token};

#[derive(Debug)]
pub enum Event {
    Say { name: Option<String>, text: String },
}

impl Event {
    pub fn process(mut self, context: &Rc<RefCell<CommandBlock>>) -> Self {
        use Event::*;
        match &mut self {
            Say { name, text } => {
                let context = context.borrow();
                let variables = context.get_variables();
                *name = name
                    .as_deref()
                    .map(|name| interpolate_string(name, &variables));
                *text = interpolate_string(text, &variables);
            }
        }
        self
    }
}

fn interpolate_string(input: &str, variables: &HashMap<String, Token>) -> String {
    let re = Regex::new(r"\[(\w+)\]").unwrap();
    let interpolated_string = re.replace_all(input, |caps: &regex::Captures| {
        let var_name = &caps[1];
        if let Some(value) = variables.get(var_name) {
            value.to_string()
        } else {
            panic!("undefined variable `{var_name}`");
        }
    });

    interpolated_string.into_owned()
}
