use std::str::FromStr;

use super::{
    token::Token,
    utils::{describe_line, parse_line},
    Pair,
};

#[derive(Debug, Clone)]
#[allow(dead_code)]
pub enum Command {
    Say {
        name: Option<String>,
        text: String,
    },
    Eat {
        food: String,
        politely: bool,
    },
    Assign {
        assign_type: AssignType,
        variable: String,
        value: Token,
    },
}

#[derive(Debug, Clone)]
pub enum AssignType {
    Define,
    GlobalAssign,
    Let,
    Assign,
}

impl FromStr for AssignType {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        use AssignType::*;
        Ok(match s {
            "define" => Define,
            "$" => GlobalAssign,
            "let" => Let,
            _ => return Err(()),
        })
    }
}

pub fn parse_command(pair: Pair) -> Command {
    use Command::*;
    use Token::*;
    let line = parse_line(pair);
    macro_rules! unknown {
        () => {
            panic!("Unknown command {}", describe_line(&line))
        };
    }
    match line.as_slice() {
        [Str(text)] => Say {
            name: None,
            text: text.clone(),
        },
        [Str(name), Str(text)] => Say {
            name: Some(name.clone()),
            text: text.clone(),
        },
        // https://github.com/rust-lang/rust/issues/51114
        [Keyword(keyword), Keyword(variable), Keyword(equals), value] if equals.eq("=") => Assign {
            assign_type: match keyword.parse() {
                Ok(assign_type) => assign_type,
                Err(_) => unknown!(),
            },
            variable: variable.clone(),
            value: value.clone(),
        },
        [Keyword(variable), Keyword(equals), value] if equals.eq("=") => Assign {
            assign_type: AssignType::Assign,
            variable: variable.clone(),
            value: value.clone()
        },
        [Keyword(keyword), Str(food), tail @ ..] if keyword.eq("eat") => Eat {
            food: food.to_owned(),
            politely: match tail {
                [Boolean(politely)] => *politely,
                _ => unknown!(),
            },
        },
        _ => unknown!(),
    }
}
