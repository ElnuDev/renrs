use super::{
    token::Token,
    utils::{describe_line, parse_line},
    Pair,
};

#[derive(Debug)]
#[allow(dead_code)]
pub enum Control {
    If { condition: bool },
}

pub fn parse_control(pair: Pair) -> Control {
    use Control::*;
    use Token::*;
    let line = parse_line(pair);
    macro_rules! unknown {
        () => {
            panic!("Unknown control {}", describe_line(&line))
        };
    }
    match line.as_slice() {
        [Keyword(control), Boolean(condition)] if control.eq("if") => If {
            condition: *condition,
        },
        _ => unknown!(),
    }
}
