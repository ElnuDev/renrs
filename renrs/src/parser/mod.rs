pub mod block;
pub mod command;
pub mod control;
pub mod event;
pub mod token;
mod utils;

pub type Pair<'a> = pest::iterators::Pair<'a, Rule>;

use debug_cell::RefCell;
use std::{fs, path::PathBuf, rc::Rc};

pub use pest::Parser;
use pest_derive::Parser;

use block::{parse_block, CommandBlock};

#[derive(Parser)]
#[grammar = "rpy.pest"]
pub struct RpyParser;

// Read file into commands
pub fn parse_file(file_path: &PathBuf) -> Rc<RefCell<CommandBlock>> {
    let unparsed_file = fs::read_to_string(file_path).expect("cannot find file");
    parse(&unparsed_file)
}

pub fn parse(script: &str) -> Rc<RefCell<CommandBlock>> {
    let file = RpyParser::parse(Rule::File, script)
        .expect("unsuccessful parse")
        .next()
        .unwrap();
    parse_block(file, None)
}
