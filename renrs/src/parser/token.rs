use super::{Pair, Rule};

// Raw script tokens
#[derive(Debug, Clone)]
pub enum Token {
    Keyword(String),
    Str(String),
    Array(Vec<Token>),
    Boolean(bool),
    Number(f64),
}

impl Token {
    pub fn print(&self) -> &str {
        use Token::*;
        match &self {
            Keyword(keyword) => keyword,
            Str(_) => "String",
            Array(_) => "Array",
            Boolean(_) => "Boolean",
            Number(_) => "Number",
        }
    }
}

impl ToString for Token {
    fn to_string(&self) -> String {
        use Token::*;
        match self {
            Keyword(_) => panic!("keywords should not be used as values"),
            Str(string) => string.to_owned(),
            Array(array) => format!(
                "[{}]",
                array
                    .iter()
                    .map(|token| token.to_string())
                    .collect::<Vec<String>>()
                    .join(", ")
            ),
            Boolean(boolean) => match boolean {
                true => "True",
                false => "False",
            }
            .to_owned(),
            Number(number) => number.to_string(),
        }
    }
}

pub fn parse_token(pair: Pair) -> Token {
    let token = pair.as_rule();
    macro_rules! contents {
        () => {
            pair.into_inner().next().unwrap()
        };
    }
    match token {
        Rule::String => {
            let contents = contents!();
            Token::Str(match contents.as_rule() {
                Rule::SingleQuoteStringData => contents.as_str().replace("\\'", "'"),
                Rule::DoubleQuoteStringData => contents.as_str().replace("\\\"", "\""),
                _ => unreachable!(),
            })
        }
        Rule::Array => {
            let contents = contents!();
            let mut array = Vec::new();
            for token in contents.into_inner() {
                array.push(parse_token(token));
            }
            Token::Array(array)
        }
        Rule::Boolean => Token::Boolean(match pair.as_str() {
            "True" => true,
            "False" => false,
            _ => unreachable!(),
        }),
        Rule::Number => Token::Number(pair.as_str().parse().unwrap()),
        Rule::Keyword => Token::Keyword(pair.as_str().to_owned()),
        _ => unreachable!(),
    }
}
