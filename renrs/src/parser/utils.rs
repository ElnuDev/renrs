use super::{
    token::{parse_token, Token},
    Pair,
};

pub fn parse_line(pair: Pair) -> Vec<Token> {
    pair.into_inner().map(parse_token).collect()
}

// Line description e.g. [String, Keyword, Array]
// Used in parse_command as feedback for invalid commands
pub fn describe_line(line: &[Token]) -> String {
    let mut description = "[".to_owned();
    let mut iter = line.iter();
    description.push_str(iter.next().unwrap().print());
    for token in iter {
        description.push_str(&format!(", {}", token.print()));
    }
    description.push(']');
    description
}
