use debug_cell::RefCell;
use std::path::PathBuf;
use std::rc::Rc;

pub mod parser;
use parser::block::{CommandBlock, CommandContext};
use parser::event::Event;
use parser::parse_file;

pub struct State {
    script: Rc<RefCell<CommandBlock>>,
}

impl State {
    pub fn from_file(file: PathBuf) -> State {
        State {
            script: parse_file(&file),
        }
    }

    fn next_command(&mut self) -> Option<CommandContext> {
        self.script.borrow_mut().next(&self.script)
    }
}

impl Iterator for State {
    type Item = Event;
    fn next(&mut self) -> Option<Self::Item> {
        while let Some(command) = self.next_command() {
            let event = command.execute();
            if event.is_some() {
                return event;
            }
        }
        None
    }
}
