use std::thread;
use std::time::Duration;
use std::{
    path::PathBuf,
    sync::{Arc, Mutex},
};

use renrs::State;

use egui_sfml::{egui, SfEgui};
use sfml::{
    graphics::{Color, RectangleShape, RenderTarget, RenderWindow, Transformable},
    system::Vector2f,
    window::{mouse, Event, Key, Style},
};

const WIDTH: f32 = 800.0;
const HEIGHT: f32 = 600.0;

pub fn run(file: PathBuf) {
    env_logger::init();
    let mut app = App::from_state(State::from_file(file));
    app.run();
}

struct App {
    state: State,
    text: String,
    chars: Arc<Mutex<usize>>,
    typing_kill: Option<Arc<Mutex<bool>>>,
    typing_done: Arc<Mutex<bool>>,
}

impl App {
    fn from_state(state: State) -> Self {
        Self {
            state,
            text: "".to_owned(),
            chars: Arc::new(Mutex::new(0)),
            typing_kill: None,
            typing_done: Arc::new(Mutex::new(true)),
        }
    }

    fn next(&mut self) {
        if let Some(renrs::parser::event::Event::Say { name, text }) = self.state.next() {
            self.text = match name {
                Some(name) => format!("{name}: {text}"),
                None => text,
            };
            self.start_typing();
        }
    }

    fn kill_typing(&mut self) {
        if let Some(kill) = &self.typing_kill {
            *kill.lock().unwrap() = true;
        }
    }

    fn interrupt_typing(&mut self) {
        self.kill_typing();
        *self.chars.lock().unwrap() = self.text.len();
        *self.typing_done.lock().unwrap() = true;
    }

    fn start_typing(&mut self) {
        // Kill previous typing thread if exists
        self.kill_typing();

        *self.chars.lock().unwrap() = 0;
        *self.typing_done.lock().unwrap() = false;

        // Set up references to be passed into thread
        let kill = {
            let kill = Arc::new(Mutex::new(false));
            self.typing_kill = Some(kill.clone());
            kill
        };
        let done = self.typing_done.clone();
        let chars = self.chars.clone();
        let len = self.text.len();

        thread::spawn(move || {
            let mut complete = false;
            for i in 0..(len + 1) {
                if *kill.lock().unwrap() {
                    break;
                }
                *chars.lock().unwrap() = i;
                thread::sleep(Duration::from_millis(50));
                complete = true;
            }
            if complete {
                *done.lock().unwrap() = true;
            }
        });
    }

    fn handle_interaction(&mut self) {
        if *self.typing_done.lock().unwrap() {
            self.next();
        } else {
            self.interrupt_typing();
        }
    }

    fn run(&mut self) {
        let mut window = RenderWindow::new(
            (WIDTH as u32, HEIGHT as u32),
            "renrs-gui",
            Style::CLOSE,
            &Default::default(),
        );
        let mut sfegui = SfEgui::new(&window);
        window.set_vertical_sync_enabled(true);

        const SIZE: f32 = 64.0;
        let mut velocity = Vector2f::new(4.0, 4.0);
        let mut shape = RectangleShape::with_size(Vector2f::new(SIZE, SIZE));
        shape.set_position(Vector2f::new(SIZE, SIZE));

        while window.is_open() {
            while let Some(event) = window.poll_event() {
                match event {
                    Event::Closed
                    | Event::KeyPressed {
                        code: Key::Escape, ..
                    } => return,
                    Event::MouseButtonPressed {
                        button: mouse::Button::Left,
                        ..
                    }
                    | Event::KeyPressed {
                        code: Key::Space, ..
                    } => self.handle_interaction(),
                    _ => {}
                }
            }

            {
                use egui::*;
                sfegui
                    .do_frame(|ctx| {
                        egui::CentralPanel::default()
                            .frame(Frame {
                                inner_margin: Margin::same(32.0),
                                ..Default::default()
                            })
                            .show(ctx, |ui| {
                                if ctx.input(|i| {
                                    i.key_pressed(Key::Space)
                                        || i.pointer.button_clicked(PointerButton::Primary)
                                }) {
                                    self.handle_interaction();
                                }
                                ui.with_layout(egui::Layout::left_to_right(Align::Max), |ui| {
                                    ui.heading(&self.text[0..*self.chars.lock().unwrap()])
                                });
                            });
                    })
                    .unwrap();
            }

            let Vector2f { x, y } = shape.position();
            if !(0.0..WIDTH - SIZE).contains(&x) {
                velocity.x *= -1.0;
            }
            if !(0.0..HEIGHT - SIZE).contains(&y) {
                velocity.y *= -1.0;
            }
            shape.set_position(Vector2f::new(x + velocity.x, y + velocity.y));

            window.clear(Color::BLACK);
            window.draw(&shape);
            sfegui.draw(&mut window, None);
            window.display();
        }
    }
}
