# <shell.nix>
{ pkgs ? import <nixpkgs> {}}:

let
  rust_overlay = import (builtins.fetchTarball "https://github.com/oxalica/rust-overlay/archive/master.tar.gz");
  pkgs = import <nixpkgs> { overlays = [ rust_overlay ]; };
  ruststable = (pkgs.rust-bin.stable.latest.default.override {
    extensions = [
      "rust-src"
    ];
  });
in with pkgs;
mkShell {
  buildInputs = [
    ruststable
    rust-analyzer
    bacon
    #pkg-config
    #openssl
  ];

  # Shell configuration for egui/eframe
  # Based on https://gist.github.com/TheSirC/8fc34ba9143c8fe4f0b1962f4d7225df
  # Will fail to find library otherwise

  nativeBuildInputs = with xorg; [
    libxcb
    libXcursor
    libXrandr
    libXi
    pkg-config
    sfml
  ] ++ [
    #python3
    libGL
    libGLU
  ];

  shellHook = ''
      export LD_LIBRARY_PATH=/run/opengl-driver/lib/:${lib.makeLibraryPath ([libGL libGLU])}
  '';
}
