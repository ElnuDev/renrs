# Control testing
if True:
    let x = "3"
    "Bob will be here in [x] seconds."
    x = "2"
    "Bob will be here in [x] seconds."
    if True:
        x = "1"
        let y = "4"
        y = "5"
        x = y
        "[x]"
    "Bob will be here in [x] seconds."
"Bob will be here in [x] seconds."
"Bob" "I will not say anything, [foo]"
define foo = "bar"
if False:
    "Bob" "W-what? Why am I saying this?"
    if True:
        "Testing 123"
"..."
"Bob" "Good."
"Bob" "Now I will tell you my favorite food..."
if True:
    "Bob" "...cereal!"
    if False:
        "Bob" "But I actually hate it."
        define bar = "potato"
        define foo = bar
"Bob sat on the bench."