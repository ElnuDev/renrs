use std::env;

fn main() -> Result<(), std::io::Error> {
    let mut path = env::current_exe()?; // /target/debug/renrs-demo
    path.pop(); // /target/debug
    path.pop(); // /target
    path.pop(); // /
    path.push("demo/demo.rpy"); // /demo/demo.rpy

    renrs_gui::run(path);

    Ok(())
}
